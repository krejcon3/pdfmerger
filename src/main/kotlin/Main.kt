import com.itextpdf.text.Document
import com.itextpdf.text.Image
import com.itextpdf.text.pdf.PdfWriter
import org.apache.pdfbox.io.MemoryUsageSetting
import org.apache.pdfbox.multipdf.PDFMergerUtility
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files

public val File.extension: String
    get() = name.substringAfterLast('.', "")

fun main(args: Array<String>) {
    val inputDir = File("res/input")
    val middleDir = File("res/middle")
    val outputDir = File("res/output")

    middleDir.listFiles().forEach { it.deleteRecursively() }
    outputDir.listFiles().forEach { it.deleteRecursively() }

    inputDir.listFiles().filter { it.isDirectory }.forEach { bookDir ->
        processBook(bookDir.name, bookDir, File("${middleDir.path}/${bookDir.name}"), outputDir)
    }
}

fun processBook(filename: String, inputDir: File, middleDir: File, outputDir: File) {
    println("=== $filename ===")
    val size = inputDir.listFiles().size
    println("Total size of files: $size")
    val digits = size.toString().length
    println("Count of digits: $digits")

    middleDir.mkdir()

    inputDir.listFiles().forEachIndexed { index, file ->
        Files.copy(
            file.toPath(),
            File("${middleDir.path}/${filename}_${index(digits, index)}.${file.extension}").toPath()
        )
    }

    middleDir.listFiles().filter { arrayOf("gif", "png", "jpeg", "jpg").contains(it.extension.toLowerCase()) }
        .forEach { file ->
            imageToPDF(file.toPath().toString(), "${middleDir.path}/${file.name}.pdf")
        }

    val merger = PDFMergerUtility()
    middleDir.listFiles().filter { it.extension.toLowerCase() == "pdf" }.forEach { file ->
        merger.addSource(file)
    }
    merger.destinationFileName = "${outputDir.path}/$filename.pdf"
    merger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly())
}

fun index(digits: Int, index: Int) = index.toString().padStart(digits, '0')

fun imageToPDF(input: String, output: String) {
    val document = Document()
    try {
        val fos = FileOutputStream(output)
        val writer: PdfWriter = PdfWriter.getInstance(document, fos)
        writer.open()
        document.open()
        val image = Image.getInstance(input)
        val indentation = 0
        val scaler = ((document.getPageSize()
            .getWidth() - document.leftMargin() - document.rightMargin() - indentation) / image.getWidth()) * 100
        image.scalePercent(scaler)
        document.add(image)
        document.close()
        writer.close()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}
